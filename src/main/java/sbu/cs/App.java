package sbu.cs;

public class App {

    /**
     * use this function for magical machine question.
     *
     * @param n     size of machine
     * @param arr   an array in size n * n
     * @param input the input string
     * @return the output string of machine
     */
    public String main(int n, int[][] arr, String input) {

        String table[][] = new String[n][n];
        Green test = new Green();
        Pink  test2 = new Pink();
        //khone aval
        switch (arr[0][0]) {
            case 1:
                table[0][0] = test.func1(input);
                break;
            case 2:
                table[0][0] = test.func2(input);
                break;
            case 3:
                table[0][0] = test.func2(input);
                break;
            case 4:
                table[0][0] = test.func2(input);
                break;
            case 5:
                table[0][0] = test.func2(input);
                break;
        }

        for (int i = 1; i < n; i++) {
            //satr 1
            switch (arr[0][i]) {
                case 1:
                    table[0][i] = test.func1(table[0][i - 1]);
                    break;
                case 2:
                    table[0][i] = test.func2(table[0][i - 1]);
                    break;
                case 3:
                    table[0][i] = test.func3(table[0][i - 1]);
                    break;
                case 4:
                    table[0][i] = test.func4(table[0][i - 1]);
                    break;
                case 5:
                    table[0][i] = test.func5(table[0][i - 1]);
                    break;
            }
            //soton 1
            switch (arr[i][0]) {
                case 1:
                    table[i][0] = test.func1(table[i - 1][0]);
                    break;
                case 2:
                    table[i][0] = test.func2(table[i - 1][0]);
                    break;
                case 3:
                    table[i][0] = test.func3(table[i - 1][0]);
                    break;
                case 4:
                    table[i][0] = test.func4(table[i - 1][0]);
                    break;
                case 5:
                    table[i][0] = test.func5(table[i - 1][0]);
                    break;
            }
        }

        for (int i = 1; i < n - 1; i++) {
            for (int j = 1; j < n - 1; j++) {
                switch (arr[i][j]) {
                    case 1:
                        table[i][j] = test.func1(table[i][j - 1]);
                        break;
                    case 2:
                        table[i][j] = test.func2(table[i][j - 1]);
                        break;
                    case 3:
                        table[i][j] = test.func3(table[i][j - 1]);
                        break;
                    case 4:
                        table[i][j] = test.func4(table[i][j - 1]);
                        break;
                    case 5:
                        table[i][j] = test.func5(table[i][j - 1]);
                        break;
                }
            }
            switch (arr[i][n - 1]) {
                case 1:
                    table[i][n - 1] = test2.func1(table[i][n - 2], table[i - 1][n - 1]);
                    break;
                case 2:
                    table[i][n - 1] = test2.func2(table[i][n - 2], table[i - 1][n - 1]);
                    break;
                case 3:
                    table[i][n - 1] = test2.func3(table[i][n - 2], table[i - 1][n - 1]);
                    break;
                case 4:
                    table[i][n - 1] = test2.func4(table[i][n - 2], table[i - 1][n - 1]);
                    break;
                case 5:
                    table[i][n - 1] = test2.func5(table[i][n - 2], table[i - 1][n - 1]);
                    break;
            }
            for (int j = 1; j < n - 1; j++) {
                switch (arr[j][i]) {
                    case 1:
                        table[j][i] = test.func1(table[j - 1][i]);
                        break;
                    case 2:
                        table[j][i] = test.func2(table[j - 1][i]);
                        break;
                    case 3:
                        table[j][i] = test.func3(table[j - 1][i]);
                        break;
                    case 4:
                        table[j][i] = test.func4(table[j - 1][i]);
                        break;
                    case 5:
                        table[j][i] = test.func5(table[j - 1][i]);
                        break;
                }
            }
            switch (arr[n - 1][i]) {
                case 1:
                    table[n - 1][i] = test2.func1(table[n - 1][i - 1], table[n - 2][i]);
                    break;
                case 2:
                    table[n - 1][i] = test2.func2(table[n - 1][i - 1], table[n - 2][i]);
                    break;
                case 3:
                    table[n - 1][i] = test2.func3(table[n - 1][i - 1], table[n - 2][i]);
                    break;
                case 4:
                    table[n - 1][i] = test2.func4(table[n - 1][i - 1], table[n - 2][i]);
                    break;
                case 5:
                    table[n - 1][i] = test2.func5(table[n - 1][i - 1], table[n - 2][i]);
                    break;
            }
        }
        switch (arr[n-1][n-1]) {
            case 1:
                table[n-1][n-1] = test2.func1(table[n-1][n-2], table[n-2][n-1]);
                break;
            case 2:
                table[n-1][n-1] = test2.func2(table[n-1][n-2], table[n-2][n-1]);
                break;
            case 3:
                table[n-1][n-1] = test2.func3(table[n-1][n-2], table[n-2][n-1]);
                break;
            case 4:
                table[n-1][n-1] = test2.func4(table[n-1][n-2], table[n-2][n-1]);
                break;
            case 5:
                table[n-1][n-1] = test2.func5(table[n-1][n-2], table[n-2][n-1]);
                break;
        }
        return table[n-1][n-1] ;
    }
}
