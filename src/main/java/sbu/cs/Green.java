package sbu.cs;

public class Green implements BlackFunction {
    @Override
    public String func1(String arg) {
        String reverse = new StringBuffer(arg).reverse().toString();
        return reverse;
    }

    @Override
    public String func2(String arg) {
        String result = "";
        int i = 0;
        while (i < arg.length()){
            char c = arg.charAt(i);
            result = result + c + c;
            i++;
        }
        return result;
    }

    @Override
    public String func3(String arg) {
        String dou = arg + arg;
        return dou;
    }

    @Override
    public String func4(String arg) {
        char a= arg.charAt(0);
        char b=arg.charAt(arg.length()-1);
        String result = "";
        result = result + b + a;
        int i=1;
        while (i < arg.length()-1){
            char c = arg.charAt(i);
            result = result + c;
            i++;
        }

        return result;
    }

    @Override
    public String func5(String arg) {
        String finaly = "";
        for (int i = 0; i < arg.length(); i++) {
            int a = arg.charAt(i) - 97 ;
            a = 'z' - a ;
            finaly = finaly + (char) (a);
        }
        return finaly;
    }

    ;

}
