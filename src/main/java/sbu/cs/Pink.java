package sbu.cs;
    public class Pink implements WhiteFunction {
        @Override
        public String func1(String arg1, String arg2) {
            String result="";
            if (arg1.length()==arg2.length()) {
                for (int i=0 ; i<arg1.length() ; i++ ) {
                    char a = arg1.charAt(i);
                    char b = arg2.charAt(i);
                    result = result + a + b;
                }
            }
            else {
                String low = "";
                String high = "";
                if (arg1.length()>arg2.length()) {
                low = arg2;
                high = arg1;
                }
                else {
                    low = arg1;
                    high = arg2;
                }
                    for (int i=0 ; i<low.length() ; i++ ) {
                        char a = low.charAt(i);
                        char b = high.charAt(i);
                        result = result + a + b;
                    }
                for (int i=low.length() ; i<high.length() ; i++ ) {
                    char b = high.charAt(i);
                    result = result + b;
                }

            }
            return result;
        }

        @Override
        public String func2(String arg1, String arg2) {
            String reverse = new StringBuffer(arg2).reverse().toString();
            String result = arg1 + reverse;
            return result;
        }

        @Override
        public String func3(String arg1, String arg2) {
            String finaly = "" ;
            int j = arg2.length() - 1 ;
            for(int i = 0 ; i < arg1.length() ; i++){
                if(j >= 0){
                    finaly = finaly + arg1.charAt(i) + arg2.charAt(j) ;
                }
                else{
                    finaly = finaly + arg1.charAt(i) ;
                }
                j-- ;
            }
            while(j >= 0){
                finaly = finaly + arg2.charAt(j);
                j-- ;
            }
            return finaly;
        }

        @Override
        public String func4(String arg1, String arg2) {
            if (arg1.length()%2==0) {
                return arg1;
            }
            else {
                return arg2;
            }
        }

        @Override
        public String func5(String arg1, String arg2) {
            StringBuilder stringBuilder = new StringBuilder();
            if (arg1.length() == arg2.length())
            {
                for (int i = 0; i < arg1.length(); i++)
                {
                    stringBuilder.append(characterMaker(arg1.charAt(i),arg2.charAt(i)));
                }
                return stringBuilder.toString();
            }
            else if (arg1.length() > arg2.length())
            {
                for (int i = 0; i < arg2.length(); i++)
                {
                    stringBuilder.append(characterMaker(arg1.charAt(i),arg2.charAt(i)));
                }
                stringBuilder.append(arg1.substring(arg2.length()));
                return stringBuilder.toString();
            }
            else
            {
                for (int i = 0; i < arg1.length(); i++)
                {
                    stringBuilder.append(characterMaker(arg1.charAt(i),arg2.charAt(i)));
                }
                stringBuilder.append(arg2.substring(arg1.length()));
                return stringBuilder.toString();
            }
        }

        private String characterMaker(char i,char j)
        {
            char c =(char)((i + j - 2*'a') % 26 + 'a');
            String result;
            result = "" + c;
            return result;
        }
    }
