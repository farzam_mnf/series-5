package sbu.cs;

public class SortArray {

    /**
     * sort array arr with selection sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] selectionSort(int[] arr, int size) {

        for (int i = 0; i < size - 1; i++) {
            int min_idx = i;
            for (int j = i + 1; j < size; j++)
                if (arr[j] < arr[min_idx])
                    min_idx = j;
            int temp = arr[min_idx];
            arr[min_idx] = arr[i];
            arr[i] = temp;
        }

        return arr;
    }

    /**
     * sort array arr with insertion sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] insertionSort(int[] arr, int size) {
        int n = arr.length;
        for (int i = 1; i < n; ++i) {
            int key = arr[i];
            int j = i - 1;

            while (j >= 0 && arr[j] > key) {
                arr[j + 1] = arr[j];
                j = j - 1;
            }
            arr[j + 1] = key;
        }

        return arr;
    }

    /**
     * sort array arr with merge sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     */
    public int[] mergeSort(int[] arr, int size) {
        mergeSort2(arr,size);
        return arr;
    }

    public static void mergeSort2(int[] arr, int size) {
        int n= size;
        if (n < 2) {
            return;
        }
        int mid = n / 2;
        int[] l = new int[mid];
        int[] r = new int[n - mid];

        for (int i = 0; i < mid; i++) {
            l[i] = arr[i];
        }
        for (int i = mid; i < n; i++) {
            r[i - mid] = arr[i];
        }
        mergeSort2(l, mid);
        mergeSort2(r, n - mid);
        merge(arr, l, r, mid, n - mid);
    }
    public static void merge(int[] arr, int[] l, int[] r, int left, int right) {

        int i = 0, j = 0, k = 0;
        while (i < left && j < right) {
            if (l[i] <= r[j]) {
                arr[k++] = l[i++];
            }
            else {
                arr[k++] = r[j++];
            }
        }
        while (i < left) {
            arr[k++] = l[i++];
        }
        while (j < right) {
            arr[k++] = r[j++];
        }
    }


    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in iterative form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearch(int[] arr, int value) {
            int index = -1;
            int low=0;
            int high= arr.length-1;

            while (low <= high) {
                int mid = (low + high) / 2;
                if (arr[mid] < value) {
                    low = mid + 1;
                } else if (arr[mid] > value) {
                    high = mid - 1;
                } else if (arr[mid] == value) {
                    index = mid;
                    break;
                }
            }
            return index;
        }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in recursive form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */

    public int binarySearchRecursive(int[] arr, int value) {
        return Bs(arr,0,arr.length-1,value);
    }
    public int Bs(int arr[], int l, int r, int x) {

        if (r >= l && l<arr.length-1) {

            int mid = l + (r - l) / 2;

            if (arr[mid] == x)
                return mid;


            if (arr[mid] > x)
                return Bs(arr, l, mid - 1, x);

            return Bs(arr, mid + 1, r, x);
        }

        return -1;
    }
}
