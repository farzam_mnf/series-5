package sbu.cs;

public interface WhiteFunction {
    public String func1(String arg1, String arg2);
    public String func2(String arg1, String arg2);
    public String func3(String arg1, String arg2);
    public String func4(String arg1, String arg2);
    public String func5(String arg1, String arg2);
}
